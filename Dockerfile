# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM ubuntu:latest
RUN echo "deb http://archive.ubuntu.com/ubuntu bionic universe" | tee -a /etc/apt/sources.list

RUN apt install tesseract-ocr
RUN apt install libtesseract-dev

# WORKDIR /usr/local/bin

# Change `app` to whatever your binary is called
# Add app .
CMD ["ping"]
